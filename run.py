from seft.cli.fit_model import main

# python run.py --dataset physionet2012 --balance --max_epochs 1 DeepSetAttentionModel
# python run.py --dataset physionet2019 --balance --max_epochs 1 DeepSetAttentionModel
# python run.py --dataset mimic3_phenotyping --balance --max_epochs 1 DeepSetAttentionModel

if __name__ == '__main__':
    main()
